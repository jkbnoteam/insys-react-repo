export const SELECT_TEXT_SITE = 'SELECT_TEXT_SITE';
export const SELECT_GALLERY_SITE = 'SELECT_GALLERY_SITE';
export const GET_PHOTOS_REQUEST = 'GET_PHOTOS_REQUEST';
export const GET_PHOTOS_SUCCESS = 'GET_PHOTOS_SUCCESS';
export const GET_PHOTOS_FAIL = 'GET_PHOTOS_FAIL';
export const SELECT_GALLERY_IMAGE = 'SELECT_GALLERY_IMAGE';
export const TOGGLE_MODAL = 'TOGGLE_MODAL';

export function selectTextSite() {
    return {
      type: SELECT_TEXT_SITE
    };
}

export function selectGallerySite() {
    return {
      type: SELECT_GALLERY_SITE
    };
}

export function getPhotosRequest() {
    return {
      type: GET_PHOTOS_REQUEST
    };
}

export function getPhotosSuccess(data) {
    return {
      type: GET_PHOTOS_SUCCESS,
      payload: data
    };
}

export function getPhotosFail(error) {
    return {
      type: GET_PHOTOS_FAIL,
      payload: error
    };
}

export function selectGalleryImage(data) {
    return {
      type: SELECT_GALLERY_IMAGE,
      payload: data
    };
}

export function toggleModal(data) {
    return {
      type: TOGGLE_MODAL,
      payload: data
    };
}