import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Texts from './Texts.js';
import Gallery from './Gallery.js';

import ImageBeach from '../imgs/sandy-beach.jpg';
import ImageMarilyn from '../imgs/marilyn-monroe.jpg';

import {
    selectTextSite,
    selectGallerySite,
    getPhotosRequest
} from '../actions/Actions';


class Navigation extends Component {
    
	components = {
		"Texts": Texts,
		"Gallery": Gallery
	};

    render(){

    	const PresentSite = this.components[this.props.appState.site];

        return (
        	<Router>
	        	<section>
		            <header className="container-fluid px-0">
				        <img className="supra-nav__image" src={ImageBeach} alt="Beach">
			        	</img>
					</header>
			      	<nav className="navbar navbar-expand-lg navbar-light navbar--color p-0 border-top border-light">
						<Link to="/texts/" className="navbar-brand mr-auto" onClick={this.selectTexts}>
							<img className="navbar-brand__image navbar-brand__image--position" src={ImageMarilyn} alt="Marilyn">
							</img>
						</Link>
						<ul className="navbar-nav d-flex flex-row p-0">
							<li className="nav-item">
								<Link to="/texts/" className="nav-link py-1 pl-0 pr-2" onClick={this.selectTexts}>
									<i className="fa fa-user-circle text-light">
									</i>
								</Link>
							</li>
							<li className="nav-item">
								<Link to="/gallery/" className="nav-link py-1 pl-0 pr-2" onClick={this.selectGallery}>
									<i className="fa fa-image text-light">
									</i>
								</Link>
							</li>
						</ul>
					</nav>

			        <Route path="/" exact component={Texts} />
			        <Route path="/about/" component={Gallery} />

					<section>
						<PresentSite />
					</section>
				</section>
			</Router>
        );
    }
    selectTexts = () => {
		this.props.selectTextSite();
	};
    selectGallery = () => {
		this.props.selectGallerySite();
		this.props.getPhotosRequest();
	};
}

function mapStateToProps(state) {
    return {
        appState: state.appState
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
	    selectTextSite,
	    selectGallerySite,
	    getPhotosRequest
    }, dispatch);
 }

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);