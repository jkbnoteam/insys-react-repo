import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

import {
    toggleModal
} from '../actions/Actions';


class ImageModal extends Component {

    render() {

    	if (this.props.appState.selectedImageLoaded) {
	    	let selectedImage = this.props.appState.selectedImage;
	    	return (
				<Modal isOpen={this.props.appState.modalOpened} toggle={() => this.toggleModal()}>
					<ModalHeader toggle={() => this.toggleModal()}>
						{selectedImage.title}
					</ModalHeader>
					<ModalBody>
						<img className="mx-auto d-block" src={selectedImage.media.m} alt="Marylin">
						</img>
					</ModalBody>
				</Modal>
	    	);
	    } else {
        	return (
				<Modal isOpen={this.props.appState.modalOpened} toggle={() => this.toggleModal()}>
					<ModalHeader toggle={() => this.toggleModal()}>
					</ModalHeader>
					<ModalBody>
					</ModalBody>
				</Modal>
	        );
        }
    }

    toggleModal = () => {
		this.props.toggleModal(false);
	}
}   

function mapStateToProps(state) {
    return {
        appState: state.appState
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
		toggleModal
    }, dispatch);
 }

export default connect(mapStateToProps, mapDispatchToProps)(ImageModal);