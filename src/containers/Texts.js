import React, { Component } from 'react';
import { connect } from 'react-redux';


class Texts extends Component {
    render(){

        return (
			<article className="container mt-5 text-left">
				<div className="row d-block">
					<h2>
						Marilyn Monroe
					</h2>
					<p className="location-info text-secondary">
						<span>
							<i className="fa fa-map-marker">
							</i>
						</span>
						<span className="pl-1">
							Poznan, PL
						</span>
					</p>
				</div>
				<p>
					At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.
				</p>
				<p className="quote-block text-secondary">
					On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish.
				</p>
				<p>
					Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.
				</p>
			</article>
        );
    }
}

function mapStateToProps(state) {
    return {
        appState: state.appState
    };
}

export default connect(mapStateToProps)(Texts);