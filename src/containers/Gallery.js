import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import ImageModal from './Modal.js'
import EmptyPhoto from '../imgs/empty-photo.jpg';

import {
    selectGalleryImage,
    toggleModal
} from '../actions/Actions';

class Gallery extends Component {

	createGalleryBlanks = () => {
		let galleryBlanks = []

		for (let i = 0; i < 9; i++) {
			galleryBlanks.push(
				<div key={i} className="col-xs-12 col-sm-6 col-md-4 gallery__img p-2">
					<img className="gallery__img--format" src={EmptyPhoto} alt="Empty">
					</img>
				</div>
			);
		}
		return galleryBlanks
	}

	createGalleryItems = () => {
		let galleryItems = []

		for (let i = 0; i < 9; i++) {
			let image = this.props.appState.photos.items[i];
			galleryItems.push(
				<button
				key={i}
				className="col-xs-12 col-sm-6 col-md-4 gallery__img p-2"
				onClick={() => this.selectImage(image)}>
					<img className="gallery__img--format" src={image.media.m} alt="Marylin">
					</img>
				</button>
			);
		}
		return galleryItems
	}

    render(){

    	if (this.props.appState.photosLoaded) {
	        return (
				<article className="container mt-5">
					<div className="row">
						{this.createGalleryItems()}
					</div>
					<ImageModal />
				</article>
	        );
        } else {
        	return (
        		<article className="container mt-5">
					<div className="row">
						{this.createGalleryBlanks()}
					</div>
				</article>
	        );
        }
    }

	selectImage = (image) => {
		this.props.selectGalleryImage(image);
		this.props.toggleModal(true);
	}
}

function mapStateToProps(state) {
    return {
        appState: state.appState
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
		selectGalleryImage,
		toggleModal
    }, dispatch);
 }

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);