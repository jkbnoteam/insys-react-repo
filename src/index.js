import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { RootSaga } from './RootSaga.js';

import './styles/index.css';

import './styles/custom.scss';
import App from './App';
import StateData from './reducers';

import * as serviceWorker from './serviceWorker';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
	StateData,
	composeEnhancer(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(RootSaga);

ReactDOM.render(
	<Provider store={store}>
        <App />
    </Provider>
	, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
