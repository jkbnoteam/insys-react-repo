import {
    SELECT_TEXT_SITE,
    SELECT_GALLERY_SITE,
    GET_PHOTOS_REQUEST,
    GET_PHOTOS_SUCCESS,
    GET_PHOTOS_FAIL,
    SELECT_GALLERY_IMAGE,
    TOGGLE_MODAL
} from '../actions/Actions';

const INITIAL_STATE = {
    site: 'Texts',
    photosLoaded: false,
    photos: {},
    photosError: {},
    selectedImage: {},
    selectedImageLoaded: false,
    modalOpened: false
};

export default function(state = INITIAL_STATE, action) {
    switch (action.type) {
        case GET_PHOTOS_REQUEST:
            return state;

        case GET_PHOTOS_SUCCESS:
            return {
                ...state,
                photosLoaded: true,
                photos: action.payload
            };

        case GET_PHOTOS_FAIL:
            return {
                ...state,
                photosLoaded: false,
                photosError: action.payload
            };

        case SELECT_TEXT_SITE:
            return {
                ...state,
            	site: 'Texts'
            };

        case SELECT_GALLERY_SITE:
            return {
                ...state,
            	site: 'Gallery'
            };

        case SELECT_GALLERY_IMAGE:
            return {
                ...state,
                selectedImage: action.payload,
                selectedImageLoaded: true
            };
        
        case TOGGLE_MODAL:
            return {
                ...state,
                modalOpened: action.payload
            };

        default:
            return state;
    }
}