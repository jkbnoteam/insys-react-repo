import { combineReducers } from 'redux';
import ReducerAppData from './ReducerAppData';

const StateData = combineReducers({
    appState: ReducerAppData
});

export default StateData;
