import React from 'react';
import 'font-awesome/css/font-awesome.min.css';

import Navigation from './containers/Navigation';

import './styles/App.css';


function App() {
	return (
		<div className="App">
			<Navigation />
		</div>
	);	
}

export default App;
