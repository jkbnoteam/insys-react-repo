import { all } from 'redux-saga/effects'
import {
    getPhotosDataSaga
} from './sagas/AppSagas';

export function* RootSaga() {
    yield all([
        getPhotosDataSaga()
    ])
}