import * as actions from '../actions/Actions';
import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects'

//proxy in package.json stores the
const ROOT_URL = "?tags=marylin+monroe&format=json&nojsoncallback=1"

//API KEY is visible in the bundle so 
// const configAxios = {
//     Key: process.env.REACT_APP_FLICKR_API_KEY,
// };

export function* getPhotosData() {
    try {
        const response = yield call(
            axios.get,
            ROOT_URL
            );
        yield put(actions.getPhotosSuccess(response.data));
    } catch (error) {
        yield put(actions.getPhotosFail(error));
    }
}

export function* getPhotosDataSaga() {
    yield takeEvery(actions.GET_PHOTOS_REQUEST, getPhotosData);
}